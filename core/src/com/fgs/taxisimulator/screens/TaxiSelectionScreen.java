package com.fgs.taxisimulator.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.fgs.taxisimulator.GameManager;
import com.fgs.taxisimulator.ScreenManager;
import com.fgs.taxisimulator.TaxiSimulator;
import com.fgs.taxisimulator.cons.Utils;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.ui.MoreAppsButton;
import com.fgs.taxisimulator.ui.RateMyAppButton;
import com.fgs.taxisimulator.ui.VehicleButton1;
import com.fgs.taxisimulator.ui.VehicleButton2;
import com.fgs.taxisimulator.ui.VehicleButton3;

public class TaxiSelectionScreen implements Screen {

    private Stage stage;
    private Table bgTable;
    private Table lblTable;
    private Table vbTable;
    private Texture bgTexture;
    private Sprite bgSprite;
    private Image bgImage;
    private Label coinsLabel;
    private LabelStyle labelStyle;

    private VehicleButton1 v1;
    private VehicleButton2 v2;
    private VehicleButton3 v3;

    private Table rateTable;
    private RateMyAppButton rateMyAppButton;
    private MoreAppsButton moreAppsButton;

    public TaxiSelectionScreen() {
        stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
        bgTable = new Table();
        lblTable = new Table();
        vbTable = new Table();
        rateTable = new Table();
        labelStyle = new LabelStyle();
        labelStyle.font = AssetsLoader.font2;
        coinsLabel = new Label(String.valueOf(TaxiSimulator.getPrefs().getTotalBalance()), labelStyle);
        lblTable.add(coinsLabel).pad(0, 240, 790, 0);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ScreenManager.getInstance().dispose(MyScreens.TAXI_SELECTION_SCREEN);
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        Gdx.input.setCatchBackKey(true);
        setBackgroundImage();
        createButtons();
        if (TaxiSimulator.actionResolver != null)
            TaxiSimulator.actionResolver.showAds(Utils.SHOW_ADS);

    }

    private void createButtons() {
        bgTable.setFillParent(true);

        bgImage = new Image(bgTexture);
        bgTable.add(bgImage);

        v1 = new VehicleButton1();
        vbTable.add(v1).pad(0, 820, 450, 0);

        v2 = new VehicleButton2();
        if (!TaxiSimulator.getPrefs().isCarUnLocked(Utils.VEHICLE2)) {
            v2.setColor(0.5f, 0.5f, 0.5f, 0.8f);
        }
        vbTable.add(v2).pad(0, 0, 450, 0);

        v3 = new VehicleButton3();
        if (!TaxiSimulator.getPrefs().isCarUnLocked(Utils.VEHICLE3)) {
            v3.setColor(0.5f, 0.5f, 0.5f, 0.8f);
        }
        vbTable.add(v3).pad(0, 0, 450, 100);

        rateTable = new Table();
        rateTable.setFillParent(true);
        moreAppsButton = new MoreAppsButton();
        rateMyAppButton = new RateMyAppButton();

        rateTable.add(rateMyAppButton).pad(150, 400, 450, 0);
        rateTable.add(moreAppsButton).pad(150, 20, 450, 0);

        stage.addActor(bgTable);
        stage.addActor(lblTable);
        stage.addActor(vbTable);
        stage.addActor(rateTable);

    }

    private void setBackgroundImage() {
        bgTexture = AssetsLoader.taxiSelectionBG;

        bgSprite = new Sprite(bgTexture);
        bgSprite.setColor(0, 0, 0, 0);
        bgSprite.setX(Utils.virtualWidth - bgSprite.getWidth() / 2);
        bgSprite.setY(Utils.virtualHeight - bgSprite.getHeight() / 2);
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

}
