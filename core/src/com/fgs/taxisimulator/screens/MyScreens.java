package com.fgs.taxisimulator.screens;

import com.badlogic.gdx.Screen;
import com.fgs.taxisimulator.TaxiSimulator;

public enum MyScreens {

	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen();
		}
	},
	TAXI_SELECTION_SCREEN {
		public Screen getScreenInstance() {
			return new TaxiSelectionScreen();
		}
	},
	SPLASH_SCREEN {
		public Screen getScreenInstance() {
			return new SplashScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
