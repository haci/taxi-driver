package com.fgs.taxisimulator.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.fgs.taxisimulator.ActionResolver;
import com.fgs.taxisimulator.GameManager;
import com.fgs.taxisimulator.ScreenManager;
import com.fgs.taxisimulator.TaxiSimulator;
import com.fgs.taxisimulator.TaxiState;
import com.fgs.taxisimulator.cons.Utils;

public class GameScreen extends InputAdapter implements Screen {

    private OrthographicCamera camera;
    private World world;
    @SuppressWarnings("unused")
    private Box2DDebugRenderer debugRenderer;
    private SpriteBatch spriteBatch;
    private Stage stage;

    @Override
    public void show() {
        world = new World(Utils.GRAVITY, true);
        camera = new OrthographicCamera(Utils.widthMeters, Utils.heightMeters);
        debugRenderer = new Box2DDebugRenderer();
        spriteBatch = new SpriteBatch();
        stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
        camera.zoom = 0.6f;

        Gdx.input.setCatchBackKey(true);

        GameManager.adIsShowed = false;
        GameManager.isFirstRun = true;
        GameManager.setTaxiState(TaxiState.NO_PASSENGER);
        GameManager.setWorld(world);
        GameManager.setCamera(camera);
        GameManager.setStage(stage);
        GameManager.resetCarVelocityEachTime();
        GameManager.createWorldContactListener();
        GameManager.createMap();
        GameManager.createPassengerSquare();
        GameManager.initFontAndStage();
        GameManager.changeCoordTexts();
        GameManager.createCar();
        GameManager.createInputs(this);
        if (TaxiSimulator.actionResolver != null)
            TaxiSimulator.actionResolver.showAds(Utils.REMOVE_ADS);
    }

    @Override
    public boolean scrolled(int amount) {
        if (amount > 0) {
            camera.zoom = camera.zoom + 0.2f;
        } else {
            camera.zoom = camera.zoom - 0.2f;
        }
        return true;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        // debugRenderer.render(world, camera.combined);

        GameManager.drawWorldSprites(delta, spriteBatch);
        GameManager.updateWorld(delta, spriteBatch);

//        if (Gdx.input.isKeyPressed(Keys.BACK)) {
//            ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
//            ScreenManager.getInstance().show(MyScreens.TAXI_SELECTION_SCREEN);
//        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if (Keys.BACK == keycode) {
            ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
            ScreenManager.getInstance().show(MyScreens.TAXI_SELECTION_SCREEN);
        }
        return super.keyDown(keycode);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

}
