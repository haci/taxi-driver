package com.fgs.taxisimulator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class TaxiPreferences {

	private String TOTAL_BALANCE = "totalbalance";

	private Preferences prefs = Gdx.app.getPreferences("taxiPrefs");

	public void unlockCar(String key, boolean lock) {
		prefs.putBoolean(key, lock);
		prefs.flush();
	}

	public boolean isCarUnLocked(String key) {
		return prefs.getBoolean(key);
	}

	// TOTAL AMOUNT
	public void saveTotalBalance(int amount) {
		prefs.putInteger(TOTAL_BALANCE, amount);
		prefs.flush();
	}

	public int getTotalBalance() {
		return prefs.getInteger(TOTAL_BALANCE);
	}

	public void clearPrefs() {
		prefs.clear();
	}

}
