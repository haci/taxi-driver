package com.fgs.taxisimulator.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.fgs.taxisimulator.GameManager;
import com.fgs.taxisimulator.ScreenManager;
import com.fgs.taxisimulator.cons.Utils;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.screens.MyScreens;

public class VehicleButton1 extends Button {
	public static int clickCounter;

	private String buttonUp;
	private Skin skin;
	private ButtonStyle style;

	public VehicleButton1() {
		buttonUp = "ButtonUp";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.btnCar1);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						buttonFunction(event);
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction(InputEvent event) {

				GameManager.selectedVehicle = Utils.VEHICLE1;

				event.getStage().addAction(
						Actions.sequence(Actions.fadeOut(.7f, Interpolation.pow5Out),
								Actions.run(new Runnable() {

									@Override
									public void run() {
										ScreenManager.getInstance().dispose(MyScreens.TAXI_SELECTION_SCREEN);
										ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
									}
								})));

				clickCounter = 0;
			}

		});
	}
}
