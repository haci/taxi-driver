package com.fgs.taxisimulator.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.fgs.taxisimulator.helpers.AssetsLoader;

public class RateMyAppButton extends Button {

	private String buttonUp;
	private Skin skin;
	private ButtonStyle style;
	private String url = "https://play.google.com/store/apps/details?id=com.fgs.taxisimulator.android";

	public RateMyAppButton() {
		buttonUp = "ButtonUp";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.rateMyApp);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					buttonFunction(event);
				}
			}

			private void buttonFunction(InputEvent event) {
				Gdx.net.openURI(url);
			}

		});
	}
}