package com.fgs.taxisimulator.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.fgs.taxisimulator.GameManager;
import com.fgs.taxisimulator.ScreenManager;
import com.fgs.taxisimulator.TaxiSimulator;
import com.fgs.taxisimulator.cons.Utils;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.screens.MyScreens;

public class VehicleButton3 extends Button {

	private String buttonUp;
	private Skin skin;
	private ButtonStyle style;

	public VehicleButton3() {
		buttonUp = "ButtonUp";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.btnCar3);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					buttonFunction(event);
				}
			}

			private void buttonFunction(InputEvent event) {

				if (TaxiSimulator.getPrefs().isCarUnLocked(Utils.VEHICLE3) == false) {
					event.getListenerActor().addAction(
							Actions.sequence(Actions.moveBy(15, 0), Actions.moveBy(-15, 0)));
					if (TaxiSimulator.getPrefs().getTotalBalance() >= Utils.VEHICLE3_COST) {
						TaxiSimulator.getPrefs().unlockCar(Utils.VEHICLE3, true);
						TaxiSimulator.getPrefs().saveTotalBalance(
								TaxiSimulator.getPrefs().getTotalBalance() - Utils.VEHICLE3_COST);
						event.getListenerActor().setColor(1, 1, 1, 1);
					}
				} else {
					GameManager.selectedVehicle = Utils.VEHICLE3;

					event.getStage().addAction(
							Actions.sequence(Actions.fadeOut(.7f, Interpolation.pow5Out),
									Actions.run(new Runnable() {

										@Override
										public void run() {
											ScreenManager.getInstance().dispose(
													MyScreens.TAXI_SELECTION_SCREEN);
											ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
										}
									})));

				}

			}

		});
	}
}
