package com.fgs.taxisimulator.vehicle;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.fgs.taxisimulator.GameManager;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.helpers.CustomSprite;

public class CarConfig {

	public final static float carWidth = 0.6f;
	public final static float carLength = 1.4f;
	public final static Vector2 carPosition = new Vector2(-GameManager.camera.viewportWidth + 3,
			GameManager.camera.viewportHeight + 14.5f);
	public final static float carAngle = (float) Math.PI;
	public final static float carMaxSteerAngle = 25;
	public final static float carPower1 = 3;
	public final static float carMaxSpeed1 = 15;
	public final static float carPower2 = 4;
	public final static float carMaxSpeed2 = 18;
	public final static float carPower3 = 5;
	public final static float carMaxSpeed3 = 22;
	public final static CustomSprite carSprite1 = new CustomSprite(AssetsLoader.carTexture1, 0.8f, 1.6f);
	public final static CustomSprite carSprite2 = new CustomSprite(AssetsLoader.carTexture2, 0.8f, 1.6f);
	public final static CustomSprite carSprite3 = new CustomSprite(AssetsLoader.carTexture3, 0.8f, 1.6f);
	public static Body carBody;

}
