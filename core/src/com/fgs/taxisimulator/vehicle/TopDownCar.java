package com.fgs.taxisimulator.vehicle;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.fgs.taxisimulator.carcontroller.VehicleBrakeButton;
import com.fgs.taxisimulator.carcontroller.VehicleGasButton;
import com.fgs.taxisimulator.carcontroller.VehicleSteeringWheel;
import com.fgs.taxisimulator.cons.Utils;

public class TopDownCar {

	public static final int STEER_NONE = 0;
	public static final int STEER_RIGHT = 1;
	public static final int STEER_LEFT = 2;

	public static final int ACC_NONE = 0;
	public static final int ACC_ACCELERATE = 1;
	public static final int ACC_BRAKE = 2;

	public static boolean acc = false;
	public static boolean dec = false;
	public static boolean non = true;

	public static boolean steerR = false;
	public static boolean steerL = false;
	public static boolean steerN = true;

	private static Car car;
	private Stage stage;
	private VehicleGasButton gasBtn;
	private VehicleBrakeButton brakeBtn;
	private VehicleSteeringWheel steeringWheel;
	private Table table;

	public TopDownCar() {
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));

		table = new Table();
		table.setFillParent(true);

		gasBtn = new VehicleGasButton();
		brakeBtn = new VehicleBrakeButton();
		steeringWheel = new VehicleSteeringWheel();

		table.add(brakeBtn).pad(400, 650, 0, 40);
		table.add(gasBtn).pad(390, -30, 0, 0);
		table.add(steeringWheel).pad(350, -1400, 0, 0);

		stage.addActor(table);

	}

	public void create(World world, float width, float length, Vector2 position, float angle, float power,
			float maxSteerAngle, float maxSpeed, Sprite carSprite) {

		car = new Car(world, width, length, position, angle, power, maxSteerAngle, maxSpeed);
		car.body.setUserData(carSprite);
	}

	private void gas() {
		car.accelerate = TopDownCar.ACC_ACCELERATE;
	}

	private void brake() {
		car.accelerate = TopDownCar.ACC_BRAKE;
	}

	private void disableMotor() {
		car.accelerate = TopDownCar.ACC_NONE;
	}

	private void steerLeft() {
		car.steer = TopDownCar.STEER_LEFT;
	}

	private void steerRight() {
		car.steer = TopDownCar.STEER_RIGHT;
	}

	private void steerNone() {
		car.steer = TopDownCar.STEER_NONE;
	}

	public void update(float delta) {

		if (Gdx.app.getType() == ApplicationType.Android) {
			mobileController(delta);
		} else {
			keyboardControllers(delta);
		}
	}

	private void mobileController(float delta) {
		if (acc) {
			gas();
		} else if (dec) {
			brake();
		} else {
			disableMotor();
		}

		if (steerL) {
			steerLeft();
		} else if (steerR) {
			steerRight();
		} else {
			steerNone();
		}
		car.update(delta);
	}

	private void keyboardControllers(float delta) {
		if (Gdx.input.isKeyPressed(Input.Keys.DPAD_UP)) {
			car.accelerate = TopDownCar.ACC_ACCELERATE;
		} else if (Gdx.input.isKeyPressed(Input.Keys.DPAD_DOWN))
			car.accelerate = TopDownCar.ACC_BRAKE;
		else
			car.accelerate = TopDownCar.ACC_NONE;

		if (Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT))
			car.steer = TopDownCar.STEER_LEFT;
		else if (Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT))
			car.steer = TopDownCar.STEER_RIGHT;
		else
			car.steer = TopDownCar.STEER_NONE;

		car.update(delta);
	}

	public Car getBaseCar() {
		return car;
	}

	public Stage getStage() {
		return stage;
	}
}
