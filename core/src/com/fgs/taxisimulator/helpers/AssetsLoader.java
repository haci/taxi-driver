package com.fgs.taxisimulator.helpers;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class AssetsLoader {

	private static FreeTypeFontGenerator generator;
	private static FreeTypeFontParameter parameter;
	public static BitmapFont font;
	public static BitmapFont font2;

	private static Sound sound;
	private static Music music;
	public static Texture screenBg;
	public static Texture carTexture1, carTexture2, carTexture3, gas, gasOn, brake, brakeOn, steeringWheel;
	public static Texture splash;
	public static Texture btnCar1, btnCar2, btnCar3;
	public static Texture cityTexture, coordBG, taxiSelectionBG;
	public static Texture rateMyApp, moreApps;

	public static Texture loadTexture(String filePath) {
		return new Texture(Gdx.files.internal(filePath));
	}

	public static Sound loadSound(String filePath) {
		sound = Gdx.audio.newSound(Gdx.files.internal(filePath));
		return sound;
	}

	public static Music loadMusic(String filePath) {
		music = Gdx.audio.newMusic(Gdx.files.internal(filePath));
		return music;
	}

	public static FileHandle loadJSON(String filePath) {
		return Gdx.files.internal(filePath);
	}

	public static void loadTexturesOnCreate() {
		// VEHICLE
		carTexture1 = loadTexture("vehicle/car1.png");
		carTexture2 = loadTexture("vehicle/car2.png");
		carTexture3 = loadTexture("vehicle/car3.png");
		gas = loadTexture("vehicle/gas.png");
		gasOn = loadTexture("vehicle/gas_on.png");
		brake = loadTexture("vehicle/brake.png");
		brakeOn = loadTexture("vehicle/brake_on.png");
		steeringWheel = loadTexture("vehicle/swheel.png");
		// STAGE
		screenBg = loadTexture("stage/taxiselectbg.jpg");
		// LOGO
		splash = loadTexture("logo.png");

		// MODELS
		cityTexture = loadTexture("maps/map.jpg");

		// IMAGES
		coordBG = loadTexture("coordbg.png");
		taxiSelectionBG = loadTexture("stage/taxiselectbg.jpg");
		btnCar1 = loadTexture("stage/car1.png");
		btnCar2 = loadTexture("stage/car2.png");
		btnCar3 = loadTexture("stage/car3.png");

		// APPS
		rateMyApp = loadTexture("rateapp.png");
		moreApps = loadTexture("moreapp.png");

		generateFont();

	}

	private static void generateFont() {
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/text.ttf"));
		parameter = new FreeTypeFontParameter();
		int density = 30;
		if (ApplicationType.Android == Gdx.app.getType()) {
			density = 5;
		}

		parameter.size = (int) (density * Gdx.graphics.getDensity());
		font = generator.generateFont(parameter);

		int density2 = 50;
		if (ApplicationType.Android == Gdx.app.getType()) {
			density2 = 10;
		}

		parameter.size = (int) (density2 * Gdx.graphics.getDensity());
		font2 = generator.generateFont(parameter);

		generator.dispose(); // don't forget to dispose to avoid memory leaks!

	}

	public static void dispose() {
		splash.dispose();
		// carTexture.dispose();
		gas.dispose();
		gasOn.dispose();
		brakeOn.dispose();
		brake.dispose();
		steeringWheel.dispose();
		screenBg.dispose();
		cityTexture.dispose();
		coordBG.dispose();
		btnCar1.dispose();
		btnCar2.dispose();
		btnCar3.dispose();
		rateMyApp.dispose();
		moreApps.dispose();
	}
}