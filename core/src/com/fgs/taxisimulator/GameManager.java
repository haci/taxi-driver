package com.fgs.taxisimulator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.fgs.taxisimulator.cons.Utils;
import com.fgs.taxisimulator.helpers.AnimatedSprite;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.vehicle.CarConfig;
import com.fgs.taxisimulator.vehicle.TopDownCar;
import com.fgs.taxisimulator.world.Square;

import java.util.ArrayList;
import java.util.Random;

public class GameManager {

    public static OrthographicCamera camera;
    public static Stage stage;
    public static int gameLevel = GameLevel.LEVEL1;
    public static TaxiState taxiState = TaxiState.NO_PASSENGER;

    private static World world;
    public static TopDownCar car;
    private static InputMultiplexer multiplexer;

    public static int adCounter = Utils.INTERSTITIAL_FREQ - 1;
    public static boolean adIsShowed = false;

    private static Square pasSq;
    private static Square desSq;
    private static Random rnd = new Random();
    private static int value;

    private static Body cityBody;
    private static Sprite citySprite;
    private static Vector2 cityOrigin;

    private static ArrayList<Body> tmpBodies = new ArrayList<Body>();

    private static Table carTable;
    private static Table pasTable;
    private static Table desTable;
    private static Table balanceTable;
    private static Label pasPos;
    private static Label desPos;
    private static Label carPos;
    private static Label lblBalance;
    private static LabelStyle labelStyle;
    private static Table bgTable;

    public static boolean isFirstRun = true;
    public static String selectedVehicle;

    private static int balance = TaxiSimulator.getPrefs().getTotalBalance();

    public static void setWorld(World w) {
        world = w;
    }

    public static void setCamera(OrthographicCamera c) {
        camera = c;
    }

    public static void setStage(Stage s) {
        stage = s;
    }

    public static void initFontAndStage() {
        // PASSENGER POS
        pasTable = new Table();
        pasTable.setFillParent(true);
        labelStyle = new LabelStyle();
        labelStyle.font = AssetsLoader.font;
        pasPos = new Label("", labelStyle);
        pasTable.add(pasPos).pad(0, 0, 430, 180);
        // DESTINATION POS
        desTable = new Table();
        desTable.setFillParent(true);
        desPos = new Label("", labelStyle);
        desTable.add(desPos).pad(0, 320, 430, 0);
        // CAR POS
        carTable = new Table();
        carTable.setFillParent(true);
        carPos = new Label("", labelStyle);
        carTable.add(carPos).pad(0, 0, 430, 550);
        // BALANCE
        balanceTable = new Table();
        balanceTable.setFillParent(true);
        lblBalance = new Label(TaxiSimulator.getPrefs().getTotalBalance() + "", labelStyle);
        balanceTable.add(lblBalance).pad(0, 670, 430, 0);
        // BACKGROUND
        bgTable = new Table();
        bgTable.setFillParent(true);
        Image img = new Image(AssetsLoader.coordBG);
        bgTable.add(img).pad(0, 0, 420, 0);

        // ADD TO STAGE
        stage.addActor(bgTable);
        stage.addActor(pasTable);
        stage.addActor(desTable);
        stage.addActor(carTable);
        stage.addActor(balanceTable);
    }

    public static void updateBalance() {
        TaxiSimulator.getPrefs().saveTotalBalance(balance);
    }

    public static void setTaxiState(TaxiState state) {
        taxiState = state;
    }

    public static void showIntersititial() {

        if (adCounter == Utils.INTERSTITIAL_FREQ) {
            if (!adIsShowed) {
                TaxiSimulator.actionResolver.showOrLoadInterstital();
                adIsShowed = true;
                adCounter--;
                if (adTimer == null)
                    createTimer();
            }
        } else if (adCounter == 0) {
            adCounter = Utils.INTERSTITIAL_FREQ;
        } else {
            adCounter--;
        }
    }

    private static Timer adTimer;

    private static void createTimer() {
        adTimer = new Timer();
        adTimer.scheduleTask(new Task() {
            @Override
            public void run() {
                adIsShowed = false;
                adTimer = null;
            }
        }, 50);
    }

    public static void createMap() {

        // 0. Create a loader for the file saved from the editor.
        BodyEditorLoader loader = new BodyEditorLoader(AssetsLoader.loadJSON("maps/cityModel.json"));

        BodyDef bd = new BodyDef();
        bd.position.set(0, 0);
        bd.type = BodyType.DynamicBody;

        FixtureDef fd = new FixtureDef();
        fd.density = 1;
        fd.friction = 0.5f;
        fd.restitution = 0.3f;

        cityBody = world.createBody(bd);
        loader.attachFixture(cityBody, "cityImage", fd, 100);

        cityOrigin = loader.getOrigin("cityImage", 100).cpy();
        Vector2 pos = cityBody.getPosition().sub(cityOrigin);

        citySprite = new Sprite(AssetsLoader.cityTexture);
        citySprite.setSize(100, 100);
        citySprite.setPosition(pos.x, pos.y);
        citySprite.setOrigin(cityOrigin.x, cityOrigin.y);

    }

    public static void resetCarVelocityEachTime() {
        TopDownCar.acc = false;
        TopDownCar.dec = false;
        TopDownCar.non = true;
        TopDownCar.steerR = false;
        TopDownCar.steerL = false;
        TopDownCar.steerN = true;

    }

    public static void createCar() {
        car = new TopDownCar();
        if (selectedVehicle.equals(Utils.VEHICLE1)) {
            car.create(world, CarConfig.carWidth, CarConfig.carLength, CarConfig.carPosition,
                    CarConfig.carAngle, CarConfig.carPower1, CarConfig.carMaxSteerAngle,
                    CarConfig.carMaxSpeed1, CarConfig.carSprite1);
            CarConfig.carBody = car.getBaseCar().body;
        } else if (selectedVehicle.equals(Utils.VEHICLE2)) {
            car.create(world, CarConfig.carWidth, CarConfig.carLength, CarConfig.carPosition,
                    CarConfig.carAngle, CarConfig.carPower2, CarConfig.carMaxSteerAngle,
                    CarConfig.carMaxSpeed2, CarConfig.carSprite2);
            CarConfig.carBody = car.getBaseCar().body;
        } else {
            car.create(world, CarConfig.carWidth, CarConfig.carLength, CarConfig.carPosition,
                    CarConfig.carAngle, CarConfig.carPower3, CarConfig.carMaxSteerAngle,
                    CarConfig.carMaxSpeed3, CarConfig.carSprite3);
            CarConfig.carBody = car.getBaseCar().body;
        }
    }

    public static void createInputs(InputAdapter adapter) {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(car.getStage());
        multiplexer.addProcessor(adapter);
        Gdx.input.setInputProcessor(multiplexer);
    }

    public static void createPassengerSquare() {
        value = rnd.nextInt(13);
        pasSq = new Square(true);
        desSq = new Square(false);
        pasSq.create(world, pasSq.getPassengerPos(value));
        desSq.create(world, desSq.getDestinationPos(value));

        if (isFirstRun == false) {
            changeCoordTexts();
        }

    }

    public static void changeCoordTexts() {
        pasPos.setText("X: " + (int) pasSq.getPassengerPos(value).x + "  Y: "
                + (int) pasSq.getPassengerPos(value).y);
        desPos.setText("X: " + (int) desSq.getDestinationPos(value).x + "  Y: "
                + (int) desSq.getDestinationPos(value).y);
        isFirstRun = false;
    }

    public static void updateWorld(float delta, SpriteBatch batch) {
        carPos.setText("X: " + (int) camera.position.x + "  Y: " + (int) camera.position.y);

        updateBodies();

        updateStages(delta);

        updateCamera();

        world.step(delta, 3, 3);
        car.update(delta);
    }

    private static void updateBodies() {
        if (tmpBodies.size() != 0) {
            for (Body body : tmpBodies) {
                world.destroyBody(body);
            }
            tmpBodies.clear();
        }

        if (taxiState == TaxiState.DROPPED_OFF_PASSENGER) {
            taxiState = TaxiState.NO_PASSENGER;
            pasPos.setText("waiting...");
            desPos.setText("waiting...");
            updateBalance();
            lblBalance.setText(TaxiSimulator.getPrefs().getTotalBalance() + "");
            Timer.schedule(new Task() {

                @Override
                public void run() {
                    createPassengerSquare();
                }
            }, 4f);
        }
    }

    private static void updateStages(float delta) {
        car.getStage().act(delta);
        car.getStage().draw();
        stage.act(delta);
        stage.draw();
    }

    private static void updateCamera() {
        camera.position.set(car.getBaseCar().getPoweredWheels().get(1).body.getPosition().x, car.getBaseCar()
                .getPoweredWheels().get(1).body.getPosition().y, 0);
        camera.update();
    }

    public static void drawWorldSprites(float delta, SpriteBatch spriteBatch) {

        // PROJECTION MATRIX
        spriteBatch.setProjectionMatrix(camera.combined);

        // CITY
        spriteBatch.begin();
        citySprite.draw(spriteBatch);
        spriteBatch.end();

        // CAR SPRITE
        Sprite carSprite = (Sprite) car.getBaseCar().body.getUserData();

        carSprite.setPosition(car.getBaseCar().body.getPosition().x - carSprite.getWidth() / 2,
                car.getBaseCar().body.getPosition().y - carSprite.getHeight() / 2);
        spriteBatch.begin();
        carSprite.draw(spriteBatch);
        carSprite.setRotation(car.getBaseCar().body.getAngle() * MathUtils.radiansToDegrees);
        spriteBatch.end();

        // PASSENGER SPRITE(ANIMATED)
        if (pasSq.getBody().getUserData() != null && pasSq.getBody().getUserData() instanceof Sprite) {
            Sprite pas = (Sprite) pasSq.getBody().getUserData();
            spriteBatch.begin();
            pas.draw(spriteBatch);
            spriteBatch.end();
        }
        // DESTINATION SPRITE(ANIMATED)
        if (desSq.getBody().getUserData() != null && desSq.getBody().getUserData() instanceof AnimatedSprite) {
            AnimatedSprite des = (AnimatedSprite) desSq.getBody().getUserData();
            spriteBatch.begin();
            des.draw(spriteBatch);
            spriteBatch.end();
        }
    }

    public static void createWorldContactListener() {

        world.setContactListener(new ContactListener() {

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }

            @Override
            public void endContact(Contact contact) {
            }

            @Override
            public void beginContact(Contact contact) {
                Fixture fixtureA = contact.getFixtureA();
                Body bodyA = contact.getFixtureA().getBody();

                checkIfCollided(fixtureA);

                if (taxiState == TaxiState.NO_PASSENGER) {
                    checkIfPickedPassenger(bodyA);
                } else if (taxiState == TaxiState.PICKED_UP_PASSENGER) {
                    checkIfDroppedPassenger(bodyA);
                }
            }

        });
    }

    private static void checkIfPickedPassenger(Body bodyA) {

        if (bodyA.equals(pasSq.getBody())) {
            taxiState = TaxiState.PICKED_UP_PASSENGER;
            tmpBodies.add(pasSq.getBody());
            pasPos.setText("OK!");
        }
    }

    private static void checkIfDroppedPassenger(Body bodyA) {
        if (bodyA.equals(desSq.getBody())) {
            taxiState = TaxiState.DROPPED_OFF_PASSENGER;
            tmpBodies.add(desSq.getBody());
            incScore(50);
        }
    }

    private static void checkIfCollided(Fixture fixtureA) {
        if (!fixtureA.isSensor()) {
//			 gameState = GameState.GAMEOVER;
            showIntersititial();
//			 ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
//			 ScreenManager.getInstance().show(MyScreens.CRASH_SCREEN);
        }
    }

    private static void incScore(int score) {
        balance += score;
    }

    public static int getCurrentLevel() {
        return gameLevel;
    }

    public static void setCurrentLevel(int level) {
        gameLevel = level;
    }

}
