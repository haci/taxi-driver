package com.fgs.taxisimulator.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.fgs.taxisimulator.helpers.AnimatedSprite;
import com.fgs.taxisimulator.helpers.AssetsLoader;

public class Square {

	private BoxProperties box;

	private Vector2[] passengerPos = { new Vector2(-28, 36), new Vector2(-28, 17), new Vector2(-8, 17),
			new Vector2(8, 17), new Vector2(18, 39), new Vector2(18, 17), new Vector2(-28, -22),
			new Vector2(-21, -30), new Vector2(21, -30), new Vector2(25, -36), new Vector2(20, -44),
			new Vector2(0, -34), new Vector2(-25, -41) };
	private Vector2[] destinationPos = { new Vector2(-25, 36), new Vector2(28, 17), new Vector2(19, -44),
			new Vector2(-28, -21), new Vector2(-18, 26), new Vector2(-38, -26), new Vector2(-28, 42),
			new Vector2(38, 36), new Vector2(-21, -44), new Vector2(-25, 36), new Vector2(-20, 44),
			new Vector2(20, 44), new Vector2(25, 32) };

	private TextureAtlas atlas;
	private Animation anim;
	private AnimatedSprite animSprite;
	private Sprite passengerSprite;
	private boolean isPassenger;

	public Square(boolean isPassenger) {
		this.isPassenger = isPassenger;
		if (!isPassenger) {
			atlas = new TextureAtlas(Gdx.files.internal("anim/stopAnim2.pack"));
			anim = new Animation(0.15f, atlas.getRegions());
			anim.setPlayMode(PlayMode.LOOP);
			animSprite = new AnimatedSprite(anim, true);
		} else {
			passengerSprite = new Sprite(AssetsLoader.loadTexture("man.png"));
		}
	}

	public void create(World world, Vector2 position) {
		box = new BoxProperties(world, 1, 1, position);
		getBody().getFixtureList().get(0).setSensor(true);
		setSprites();
	}

	private void setSprites() {

		if (isPassenger) {
			passengerSprite.setOriginCenter();
			passengerSprite.setSize(0.6f, 0.6f);
			passengerSprite.setPosition(getBody().getPosition().x - passengerSprite.getWidth() / 2, getBody()
					.getPosition().y - passengerSprite.getHeight() - 0.3f);
			getBody().setUserData(passengerSprite);
		} else {
			animSprite.setOriginCenter();
			animSprite.setSize(1, 1);
			animSprite.setPosition(getBody().getPosition().x - animSprite.getWidth() / 2, getBody()
					.getPosition().y - animSprite.getHeight() / 2);
			getBody().setUserData(animSprite);
		}
	}

	public AnimatedSprite getAnimSprite() {
		return animSprite;
	}

	public Vector2 getPassengerPos(int index) {
		return passengerPos[index];
	}

	public Vector2 getDestinationPos(int index) {
		return destinationPos[index];
	}

	public Body getBody() {
		return box.getBody();
	}
}
