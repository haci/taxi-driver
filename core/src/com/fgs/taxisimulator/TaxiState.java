package com.fgs.taxisimulator;

public enum TaxiState {
	PICKED_UP_PASSENGER, DROPPED_OFF_PASSENGER, NO_PASSENGER, FIRST_DRIVE
}
