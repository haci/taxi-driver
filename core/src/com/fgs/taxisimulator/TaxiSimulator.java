package com.fgs.taxisimulator;

import com.badlogic.gdx.Game;
import com.fgs.taxisimulator.cons.Utils;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.screens.MyScreens;

public class TaxiSimulator extends Game {
    private static TaxiPreferences prefs;
    public static ActionResolver actionResolver;

    @Override
    public void create() {
        prefs = new TaxiPreferences();
        AssetsLoader.loadTexturesOnCreate();
        prefs.unlockCar(Utils.VEHICLE1, true);
        ScreenManager.getInstance().initialize(this);
        ScreenManager.getInstance().show(MyScreens.SPLASH_SCREEN);

    }

    public TaxiSimulator(ActionResolver actionResolver) {
        TaxiSimulator.actionResolver = actionResolver;
    }

    public static TaxiPreferences getPrefs() {
        return prefs;
    }

    @Override
    public void dispose() {
        ScreenManager.getInstance().dispose();
        AssetsLoader.dispose();
    }
}
