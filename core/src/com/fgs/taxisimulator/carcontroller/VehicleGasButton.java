package com.fgs.taxisimulator.carcontroller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.vehicle.TopDownCar;

public class VehicleGasButton extends Button {

	private String buttonUp;
	private String buttonOver;
	private Skin skin;
	private ButtonStyle style;

	public VehicleGasButton() {
		buttonUp = "ButtonUp";
		buttonOver = "ButtonOver";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.gas);
		skin.add(buttonOver, AssetsLoader.gasOn);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				TopDownCar.dec = false;
				TopDownCar.acc = true;
				TopDownCar.non = false;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				TopDownCar.dec = false;
				TopDownCar.acc = false;
				TopDownCar.non = true;
			}
		});
	}

}
