package com.fgs.taxisimulator.carcontroller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.vehicle.TopDownCar;

public class VehicleBrakeButton extends Button {

	private String buttonUp;
	private String buttonOver;
	private Skin skin;
	private ButtonStyle style;

	public VehicleBrakeButton() {
		buttonUp = "ButtonUp";
		buttonOver = "Buttonover";
		initSkins();
		setButtonStyle();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.brake);
		skin.add(buttonOver, AssetsLoader.brakeOn);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over=skin.getDrawable(buttonOver);
		style.down=skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				TopDownCar.dec = true;
				TopDownCar.acc = false;
				TopDownCar.non = false;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				TopDownCar.dec = false;
				TopDownCar.acc = false;
				TopDownCar.non = true;
			}

		});
	}

}
