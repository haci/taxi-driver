package com.fgs.taxisimulator.carcontroller;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.fgs.taxisimulator.helpers.AssetsLoader;
import com.fgs.taxisimulator.vehicle.TopDownCar;

public class VehicleSteeringWheel extends Button {

	private String buttonUp;
	private String buttonOver;
	private Skin skin;
	private ButtonStyle style;
	private Vector2 touchPosition = new Vector2();

	public VehicleSteeringWheel() {
		buttonUp = "ButtonUp";
		buttonOver = "ButtonOver";
		initSkins();
		setButtonStyle();
		clickListener();
		setTransform(true);
	}

	private void initSkins() {
		skin = new Skin();
		skin.add(buttonUp, AssetsLoader.steeringWheel);
		skin.add(buttonOver, AssetsLoader.steeringWheel);
	}

	public void setButtonStyle() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
				Actor actor = event.getListenerActor();
				actor.setOrigin(getWidth() / 2, getHeight() / 2);
				if (touchPosition.x > x) {
					TopDownCar.steerL = true;
					TopDownCar.steerR = false;
					TopDownCar.steerN = false;
					if (actor.getRotation() < 70) {
						actor.addAction(Actions.rotateBy(3));
					}

				} else if (touchPosition.x < x) {
					TopDownCar.steerR = true;
					TopDownCar.steerL = false;
					TopDownCar.steerN = false;
					if (actor.getRotation() > -70) {
						actor.addAction(Actions.rotateBy(-3));
					}

				}
			}

			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				touchPosition.set(x, y);
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				TopDownCar.steerR = false;
				TopDownCar.steerL = false;
				TopDownCar.steerN = true;
				Actor actor = event.getListenerActor();
				actor.addAction(Actions.rotateTo(0, 0.2f));
			}

		});
	}
}
