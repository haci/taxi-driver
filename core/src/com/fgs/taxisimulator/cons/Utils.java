package com.fgs.taxisimulator.cons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Utils {

    public static final String VEHICLE1 = "v1";
    public static final String VEHICLE2 = "v2";
    public static final String VEHICLE3 = "v3";

    public static final int VEHICLE2_COST = 25000;
    public static final int VEHICLE3_COST = 55000;

    public static final int SHOW_ADS = 0;
    public static final int REMOVE_ADS = 1;

    public static final int INTERSTITIAL_FREQ = 1;

    private static final float offset = 0.5f;
    private static final float width = 75;

    public static float realWidth = Gdx.graphics.getWidth();
    public static float realHeight = Gdx.graphics.getHeight();

    public static final float virtualWidth = 800;
    public static final float virtualHeight = 480;

    private static final float height = 75 * (virtualHeight / virtualWidth);

    public static final float widthMeters = width / 2 - offset;
    public static final float heightMeters = height / 2 - offset;

    public static final Vector2 GRAVITY = new Vector2(0, 0);

}
