package com.fgs.taxisimulator.android;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.fgs.taxisimulator.ActionResolver;
import com.fgs.taxisimulator.TaxiSimulator;
import com.fgs.taxisimulator.cons.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class AndroidLauncher extends AndroidApplication implements ActionResolver {

    private static final String AD_UNIT_ID = "ca-app-pub-7468619672527075/4934134941";
    private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-7468619672527075/6410868144";
    private static final String TEST_DEVICE_ID = "8E452640BC83C672B070CDCA8AB9B06B";
    private AdView adView;
    private View gameView;
    private RelativeLayout layout;
    private InterstitialAd interstitialAd;

    @Override
    public void showAds(int show) {
        handler.sendEmptyMessage(show);
    }

    protected Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Utils.SHOW_ADS: {
                    adView.setVisibility(View.VISIBLE);
                    break;
                }
                case Utils.REMOVE_ADS: {
                    adView.setVisibility(View.GONE);
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new TaxiSimulator(this), config);

        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(AD_UNIT_ID);

        layout = new RelativeLayout(this);

        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        RelativeLayout.LayoutParams gameParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        gameParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        gameParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(TEST_DEVICE_ID).build();

        adView.loadAd(adRequest);

        gameView = initializeForView(new TaxiSimulator(this), config);

        layout.addView(gameView, gameParams);
        layout.addView(adView, adParams);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(AD_UNIT_ID_INTERSTITIAL);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdClosed() {
                AdRequest interstitialRequest = new AdRequest.Builder().build();
                interstitialAd.loadAd(interstitialRequest);
            }
        });

        showOrLoadInterstital();

        setContentView(layout);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adView != null) {
            adView.pause();
        }

    }

    /**
     * Called before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
    }

    @Override
    public void showOrLoadInterstital() {

        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (interstitialAd.isLoaded()) {
                        interstitialAd.show();
                    } else {
                        AdRequest interstitialRequest = new AdRequest.Builder().build();
                        interstitialAd.loadAd(interstitialRequest);
                    }
                }
            });
        } catch (Exception e) {
        }
    }
}